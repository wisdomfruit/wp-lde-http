submodule-init:
	@echo ---- setting submodule ----
	git submodule update --init
up:
	@echo ---- running containers ----
	docker compose up -d
down:
	@echo ---- stopping containers ----
	docker compose down
rerun: 
	@echo ---- stopping containers ----
	docker compose down
	@echo ---- starting containers ----
	docker compose up -d
purge:
	@echo ---- removing everything ----
	docker compose down --rmi all -v
tail:
	@echo ---- container logs ----
	docker compose logs -f
ps:
	@echo ---- currently running containers ----
	docker compose ps